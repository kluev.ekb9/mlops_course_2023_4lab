import src

RAW_DATA_PATH = "data/raw/test.csv"
NUMERIC_DATA_PATH = "data/interim/data_numeric.csv"
CLEANED_DATA_PATH = "data/interim/data_cleaned.csv"
RATIONING_DATA_PATH = "data/interim/data_rationing.csv"

if __name__ == "__main__":
    src.cat_to_num(RAW_DATA_PATH, NUMERIC_DATA_PATH)
    src.clean_data(NUMERIC_DATA_PATH, CLEANED_DATA_PATH)
    src.data_rationing(CLEANED_DATA_PATH, RATIONING_DATA_PATH)
