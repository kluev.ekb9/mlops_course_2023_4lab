import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def cat_to_num(input_path: str, output_path: str):
    df = pd.read_csv(input_path)
    # Заменяем уникальные значения для построения графиков в area_code, voice_mail_plan, international_plan, churn
    df.area_code = df.area_code.map(
        {"area_code_415": 415, "area_code_408": 408, "area_code_510": 510}
    )
    df = df.replace({"voice_mail_plan": {"yes": 1, "no": 0}})
    df = df.replace({"international_plan": {"yes": 1, "no": 0}})
    df = df.replace({"churn": {"yes": 1, "no": 0}})
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    cat_to_num()
