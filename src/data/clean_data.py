import click
import pandas as pd


# @click.command()
# @click.argument('input_path', type=click.Path(exists=True))
# @click.argument('output_path', type=click.Path())
def clean_data(input_path: str, output_path: str):
    df = pd.read_csv(input_path)
    df = df.drop(
        columns=[
            "state",
            "number_vmail_messages",
            "total_day_charge",
            "total_eve_charge",
            "total_night_charge",
            "total_intl_charge",
        ]
    )
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    clean_data()
