import click
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder

# @click.command()
# @click.argument('input_path', type=click.Path(exists=True))
# @click.argument('output_path', type=click.Path())
def data_rationing(input_path: str, output_path: str):
    df = pd.read_csv(input_path)

    scaler = StandardScaler()
    df_norm = scaler.fit_transform(df)
    df_norm = pd.DataFrame(df_norm, columns=df.columns)
    df.to_csv(output_path, index=False)

if __name__ == "__main__":
    data_rationing()