import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
def show_dataframe_info(input_path: str):
    df = pd.read_csv(input_path)
    print(df.head(), "\n" * 3)
    print(df.info(), "\n" * 3)
    print(df.isnull().sum(), "\n" * 3)


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
def show_type_col(input_path: str, type_columns):
    df = pd.read_csv(input_path)
    print(df.loc[:, type_columns].head(), "\n" * 3)
    for type_col in type_columns:
        print(df[type_col].unique())


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
# Используется для категориальных признаков
def show_unique_values(input_path: str, type_columns):
    df = pd.read_csv(input_path)
    for type_col in type_columns:
        print(df[type_col].unique())
