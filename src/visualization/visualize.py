import click
import numpy as np  # библиотека Numpy для операций линейной алгебры и прочего
import matplotlib.pyplot as plt  # библиотека MatPlotLib для визуализации
import seaborn as sns
import pandas as pd

sns.set()  # библиотека Seaborn для визуализации данных из Pandas


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
def bar_chart_single(input_path: str, target):
    df = pd.read_csv(input_path)
    fig, ax = plt.subplots(figsize=(4, 3))
    sns.countplot(x=target, data=df, hue=target, palette=["green", "red"], ax=ax)
    plt.show()


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
def bar_chart_map(input_path: str, target, second_feature):
    df = pd.read_csv(input_path)
    g = sns.FacetGrid(df, col=target, sharey=False, height=4, aspect=1.2)
    g.map(
        plt.hist,
        second_feature,
        bins=range(df[second_feature].min(), df[second_feature].max() + 1, 1),
        alpha=0.7,
    )
    g.set_axis_labels(
        "Количество звонков в службу поддержки клиентов", "Колличество обращений"
    )
    plt.subplots_adjust(top=0.82)
    plt.show()


def data_type(dframe):
    cat_columns = []  # создаем пустой список для имен колонок категориальных данных
    num_columns = []  # создаем пустой список для имен колонок числовых данных

    for column_name in dframe.columns:  # смотрим на все колонки в датафрейме
        if (
            dframe[column_name].dtypes == object
        ):  # проверяем тип данных для каждой колонки
            cat_columns += [
                column_name
            ]  # если тип объект - то складываем в категориальные данные
        else:
            num_columns += [column_name]  # иначе - числовые

    # важно: если признак категориальный, но хранится в формате числовых данных, тогда код не сработает корректно

    return cat_columns, num_columns


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
def num_feature_graph(input_path: str, num_columns):
    df = pd.read_csv(input_path)

    fig = plt.figure(figsize=(16, 8))
    fig.suptitle("Графики числовых  признаков", fontsize=18, fontweight="bold")
    fig.subplots_adjust(top=0.92)
    fig.subplots_adjust(hspace=0.7, wspace=0.6)
    width = 5
    height = int(np.ceil(len(num_columns) / width))

    for idx, column_name in enumerate(num_columns):
        ax1 = plt.subplot(height, width, idx + 1)
        sns.histplot(data=df, x=column_name, ax=ax1)
        ax1.set_title(f"{column_name}")
        ax1.set_xlabel(f"{column_name}")
        ax1.set_ylabel("Count")
    plt.show()


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
def correlation(input_path: str):
    df = pd.read_csv(input_path)

    ax = round(df.corr()["churn"].sort_values(ascending=False)[1:], 2).plot(
        kind="bar", color="dodgerblue", figsize=(10, 8)
    )
    ax.bar_label(ax.containers[0])
    plt.show()


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
def matrix_correlation(input_path: str):
    df = pd.read_csv(input_path)

    fig = plt.figure(figsize=[7, 7])
    corr_matrix = df.corr()  # вычисление матрицы корреляции
    sns.heatmap(corr_matrix)
    plt.title("Матрица коререляции датафрейма")
    plt.show()


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
def vmail_cor(input_path: str):
    df = pd.read_csv(input_path)

    plt.figure(figsize=(8, 4))
    ax = sns.histplot(data=df, x="number_vmail_messages", bins=20)
    plt.title("Матрица коререляции")
    ax.set_xlabel("number_vmail_messages")
    ax.set_ylabel("Count")
    plt.show()
